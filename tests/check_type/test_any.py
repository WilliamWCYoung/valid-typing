import typing

from valid_typing.check_type import check_type


def test_builtin():
    assert check_type(int, typing.Any)


def test_any():
    assert check_type(typing.Any, typing.Any)


def test_typing():
    assert check_type(typing.List, typing.Any)
