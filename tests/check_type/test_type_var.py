import typing

from valid_typing.check_type import check_type


def test_type_var():
    assert check_type(int, typing.TypeVar("T"))
