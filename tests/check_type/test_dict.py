import typing

from valid_typing.check_type import check_type


def test_builtin():
    assert check_type(dict, dict)


def test_typing():
    assert check_type(typing.Dict, typing.Dict)


def test_from_builtin_to_typing():
    assert check_type(dict, typing.Dict)


def test_from_typing_to_builtin():
    assert check_type(typing.Dict, dict)


def test_from_builtin_to_typing_stricter():
    assert not check_type(list, typing.Dict[str, int])


def test_from_builtin_to_typing_looser():
    assert check_type(typing.Dict[str, int], dict)


def test_stricter():
    assert check_type(typing.Dict[str, int], typing.Dict)


def test_looser():
    assert not check_type(typing.Dict, typing.Dict[str, int])
