import datetime
import typing

from valid_typing.check_type import check_type


def test_union():
    assert check_type(datetime.date, typing.Union[datetime.datetime, datetime.date])
