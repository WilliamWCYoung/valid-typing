import datetime
import typing

from valid_typing.check_type import check_type


def test_builtin_type_same():
    assert check_type(int, int)


def test_builtin_type_different():
    assert not check_type(int, str)


def test_typing_list_same_builtin_type():
    assert check_type(typing.List[int], typing.List[int])


def test_typing_list_different_builtin_type():
    assert not check_type(typing.List[int], typing.List[str])


def test_typing_union_same():
    assert check_type(str, typing.Union[str])


def test_typing_union_different():
    assert not check_type(int, typing.Union[str])


def test_typing_union_same_multiple():
    assert check_type(int, typing.Union[int, str])


def test_typing_union_in_subset():
    assert check_type(typing.Union[int, str], typing.Union[int, str, bool])


def test_typing_union_out_subset():
    assert not check_type(typing.Union[int, float], typing.Union[int, str, bool])


def test_typing_union_different_multiple():
    assert not check_type(bool, typing.Union[str, typing.Union[float, int]])


def test_nested_union_same():
    assert check_type(int, typing.Union[str, typing.Union[float, int]])


def test_typing_list_multiple_different():
    assert not check_type(
        typing.List[typing.Union[int, str]], typing.List[typing.Union[int, bool]]
    )


def test_typing_list_multiple_same():
    assert check_type(
        typing.List[typing.Union[int, str]], typing.List[typing.Union[int, str]]
    )


def test_typing_list_multiple_same_subset():
    assert check_type(
        typing.List[typing.Union[int, str]], typing.List[typing.Union[int, str, bool]]
    )


def test_nested_list_same():
    assert check_type(typing.List[typing.List[int]], typing.List[typing.List[int]])


def test_nested_list_different():
    assert not check_type(typing.List[typing.List[int]], typing.List[typing.List[str]])


def test_union_of_lists_same():
    assert check_type(
        typing.Union[typing.List[int], typing.List[str]],
        typing.Union[typing.List[int], typing.List[str]],
    )


def test_union_of_lists_different():
    assert not check_type(
        typing.Union[typing.List[int], typing.List[str]],
        typing.Union[typing.List[int], typing.List[bool]],
    )


def test_list_more_strict_bad():
    assert not check_type(typing.List, typing.List[int])


def test_list_more_strict_good():
    assert check_type(typing.List[int], typing.List)


def test_subclass():
    assert check_type(datetime.datetime, datetime.date)
