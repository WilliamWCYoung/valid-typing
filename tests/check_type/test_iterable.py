import typing

from valid_typing.check_type import check_type


def test_string():
    assert check_type(str, typing.Iterable)


def test_generic_iterable_string():
    T = typing.TypeVar("T")
    assert check_type(str, typing.Iterable[T])


def test_list():
    assert check_type(typing.List, typing.Iterable)


def test_list_generic():
    T = typing.TypeVar("T")
    assert check_type(typing.List[str], typing.Iterable[T])
