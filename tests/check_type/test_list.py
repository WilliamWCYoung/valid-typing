import typing

from valid_typing.check_type import check_type


def test_builtin():
    assert check_type(list, list)


def test_typing():
    assert check_type(typing.List, typing.List)


def test_from_builtin_to_typing():
    assert check_type(list, typing.List)


def test_from_typing_to_builtin():
    assert check_type(typing.List, list)


def test_from_builtin_to_typing_stricter():
    assert not check_type(list, typing.List[int])


def test_from_builtin_to_typing_looser():
    assert check_type(typing.List[int], list)


def test_stricter():
    assert check_type(typing.List[str], typing.List)


def test_looser():
    assert not check_type(typing.List, typing.List[str])
