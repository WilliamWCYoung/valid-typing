import typing

from valid_typing.check_type import check_type


def test_builtin():
    assert check_type(set, set)


def test_typing():
    assert check_type(typing.Set, typing.Set)


def test_from_builtin_to_typing():
    assert check_type(set, typing.Set)


def test_from_typing_to_builtin():
    assert check_type(typing.Set, set)


def test_from_builtin_to_typing_stricter():
    assert not check_type(set, typing.Set[int])


def test_from_builtin_to_typing_looser():
    assert check_type(typing.Set[int], set)


def test_stricter():
    assert check_type(typing.Set[str], typing.Set)


def test_looser():
    assert not check_type(typing.Set, typing.Set[str])
