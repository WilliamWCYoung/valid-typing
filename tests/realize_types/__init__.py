import typing

from valid_typing.check_type import check_type
from valid_typing.realize_types import realize_types


def assert_typing_equality(a, b):
    assert check_type(a, b)
    assert check_type(b, a)


def test_simple():
    T = typing.TypeVar("T")
    return_type = realize_types(str, T, T)
    assert_typing_equality(return_type, str)
