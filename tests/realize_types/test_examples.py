import decimal
import typing

from tests.realize_types import assert_typing_equality
from valid_typing.realize_types import realize_types


def test_return_type():
    T = typing.TypeVar("T")
    actual = typing.Union[int, float, decimal.Decimal]
    definition = typing.Optional[T]
    return_hint = typing.Optional[T]
    return_type = realize_types(actual, definition, return_hint)

    assert_typing_equality(return_type, typing.Optional[actual])
