import typing

from tests.realize_types import assert_typing_equality
from valid_typing.realize_types import realize_types


def test_recursive_ordering():
    T = typing.TypeVar("T")
    Q = typing.TypeVar("Q")
    return_type = realize_types(
        typing.Union[str, int], typing.Union[T, Q], typing.Union[T, Q]
    )
    assert_typing_equality(return_type, typing.Union[str, int])
