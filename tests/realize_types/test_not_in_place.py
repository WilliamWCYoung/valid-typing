import typing

from valid_typing.realize_types import realize_types

Q = typing.TypeVar("Q")


def foo(
    bar: typing.Union[str, typing.Iterable[Q]]
) -> typing.Union[str, typing.Iterable[Q]]:
    ...


def test_not_in_place():
    argument_hints = typing.get_type_hints(foo)
    realize_types(list[int], argument_hints["bar"], argument_hints["return"])

    assert argument_hints["bar"].__args__[1].__args__[0] == Q
