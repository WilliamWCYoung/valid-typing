import typing

from tests.realize_types import assert_typing_equality
from valid_typing.realize_types import realize_types


def test_return_type():
    T = typing.TypeVar("T")
    return_type = realize_types(str, T, typing.Iterable[T])
    assert_typing_equality(return_type, typing.Iterable[str])


def test_recursive():
    T = typing.TypeVar("T")
    return_type = realize_types(typing.Iterable[str], typing.Iterable[T], T)
    assert_typing_equality(return_type, str)
