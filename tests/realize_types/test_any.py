import typing

from tests.realize_types import assert_typing_equality
from valid_typing.realize_types import realize_types


def test_cant_confer_with_any():
    return_type = realize_types(str, typing.Any, typing.Any)
    assert_typing_equality(return_type, typing.Any)


def test_any_regression():
    return_type = realize_types(typing.Any, typing.Any, typing.Any)
    assert_typing_equality(return_type, typing.Any)
